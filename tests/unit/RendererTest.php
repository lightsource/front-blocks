<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Test\Unit;
use LightSource\FrontBlocks\Block\Block;
use LightSource\FrontBlocks\Block\Template;
use LightSource\FrontBlocks\Interfaces\BlockInterface;
use LightSource\FrontBlocks\Interfaces\ResourceCreatorInterface;
use LightSource\FrontBlocks\Interfaces\ResourceInterface;
use LightSource\FrontBlocks\Interfaces\TemplateInterface;
use LightSource\FrontBlocks\Interfaces\TwigInterface;
use UnitTester;
use org\bovigo\vfs\vfsStream;

class RendererTest extends Unit
{
    protected UnitTester $tester;

    private function getRendererForUsedResourcesTest(string $buttonCss = '.button{}'): Renderer
    {
        $rootDirectory   = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace       = $this->tester->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());
        $blocksFolder    = vfsStream::create(
            [
                'Button' => [
                    'Button.php' => $this->tester->getBlockClassFile(
                        $namespace . '\Button',
                        'Button',
                        '\\' . Block::class
                    ),
                    'Button.css' => $buttonCss,
                ],
                'Form'   => [
                    'Form.php' => $this->tester->getBlockClassFile(
                        $namespace . '\Form',
                        'Form',
                        '\\' . Block::class
                    ),
                    'Form.css' => '.form{}',
                ],
            ],
            $rootDirectory
        );
        $formClass       = $namespace . '\Form\Form';
        $form            = new $formClass();
        $buttonClass     = $namespace . '\Button\Button';
        $button          = new $buttonClass();
        $twig            = $this->makeEmpty(TwigInterface::class, [
            'mergeArgsRecursively' => function ($a, $b) {
                return array_merge($a, $b);
            }
        ]);
        $template        = $this->makeEmpty(TemplateInterface::class, [
            'getArgs' => function () {
                return [
                    Template::KEY_TEMPLATE  => 'my-template',
                ];
            },
        ]);
        $resourceCreator = $this->makeEmpty(ResourceCreatorInterface::class, [
            'createResource' => function (string $class) use ($namespace, $blocksFolder) {
                if ($class === $namespace . '\Form\Form') {
                    return $this->makeEmpty(ResourceInterface::class, [
                        'getFolder'       => function () use ($blocksFolder) {
                            return $blocksFolder->url();
                        },
                        'getRelativePath' => function () {
                            return 'Form/Form';
                        },
                        'getName'         => function () {
                            return 'Form';
                        },
                    ]);
                }

                return $this->makeEmpty(ResourceInterface::class, [
                    'getFolder'       => function () use ($blocksFolder) {
                        return $blocksFolder->url();
                    },
                    'getRelativePath' => function () {
                        return 'Button/Button';
                    },
                    'getName'         => function () {
                        return 'Button';
                    },
                ]);
            },
        ]);
        $renderer        = new Renderer($twig, $template, $resourceCreator);

        $renderer->render($button);
        $renderer->render($form);

        return $renderer;
    }

    private function getRendererForRenderTest(): Renderer
    {
        return new Renderer(
            $this->makeEmpty(TwigInterface::class, [
                'mergeArgsRecursively' => function ($a, $b) {
                    return array_merge($a, $b);
                }
            ]),
            $this->makeEmpty(TemplateInterface::class, [
                'getArgs' => function () {
                    return [
                        Template::KEY_TEMPLATE  => '',
                    ];
                }
            ]),
            $this->makeEmpty(ResourceCreatorInterface::class)
        );
    }

    public function testGetUsedResources()
    {
        $renderer = $this->getRendererForUsedResourcesTest();

        $this->assertEquals('.button{}.form{}', $renderer->getUsedResources('.css'));
    }

    public function testGetUsedResourcesWithInlineSource()
    {
        $renderer = $this->getRendererForUsedResourcesTest();

        $this->assertEquals(
            "\n/* Button */\n.button{}\n/* Form */\n.form{}",
            $renderer->getUsedResources('.css', true)
        );
    }

    public function testGetUsedResourcesWithoutEmptyFiles()
    {
        $renderer = $this->getRendererForUsedResourcesTest('');

        $this->assertEquals(
            "\n/* Form */\n.form{}",
            $renderer->getUsedResources('.css', true)
        );
    }

    public function testRenderAddsBlockToUsedList()
    {
        $renderer = $this->getRendererForRenderTest();
        $button   = new class extends Block {
        };

        $renderer->render($button);

        $this->assertEquals(
            [
                get_class($button),
            ],
            $renderer->getUsedBlockClasses()
        );
    }

    public function testRenderAddsBlockToUsedListOnce()
    {
        $renderer = $this->getRendererForRenderTest();
        $button   = new class extends Block {
        };

        $renderer->render($button);
        $renderer->render($button);

        $this->assertEquals(
            [
                get_class($button),
            ],
            $renderer->getUsedBlockClasses()
        );
    }

    public function testRenderAddsBlockDependenciesToUsedListBeforeTheBlock()
    {
        $renderer = $this->getRendererForRenderTest();

        // don't use makeEmpty() here, because mocks have 1 class for one mock object, so buttonClass==formClass
        $button = new class extends Block {
        };
        $form   = $this->makeEmpty(BlockInterface::class, [
            'getDependencies' => function () use ($button) {
                return [$button,];
            }
        ]);

        $renderer->render($form);

        $this->assertEquals(
            [
                get_class($button),
                get_class($form),
            ],
            $renderer->getUsedBlockClasses()
        );
    }

    public function testRenderAddsBlockDependenciesToUsedListOnce()
    {
        $renderer = $this->getRendererForRenderTest();

        // don't use makeEmpty() here, because mocks have 1 class for one mock object, so buttonClass==formClass
        $button = new class extends Block {
        };
        $form   = $this->makeEmpty(BlockInterface::class, [
            'getDependencies' => function () use ($button) {
                return [$button,];
            }
        ]);

        $renderer->render($form);
        $renderer->render($button);

        $this->assertEquals(
            [
                get_class($button),
                get_class($form),
            ],
            $renderer->getUsedBlockClasses()
        );
    }

    public function testRenderAddsBlockDependenciesToUsedListRecursively()
    {
        $renderer = $this->getRendererForRenderTest();

        global $dd;
        // don't use makeEmpty() here, because mocks have 1 class for one mock object, so buttonClass==formClass
        $button = new class extends Block {
        };
        $dd     = $button;
        $form   = new class extends Block {
            public function getDependencies(): array
            {
                global $dd;

                return [$dd];
            }
        };
        $header = $this->makeEmpty(BlockInterface::class, [
            'getDependencies' => function () use ($form) {
                return [$form,];
            }
        ]);

        $renderer->render($header);

        $this->assertEquals(
            [
                get_class($button),
                get_class($form),
                get_class($header),
            ],
            $renderer->getUsedBlockClasses()
        );

        unset($dd);
    }

    public function testRenderHasClearChainAfterAddsBlockDependenciesToUsedListRecursively()
    {
        $renderer = $this->getRendererForRenderTest();

        global $dd;
        // don't use makeEmpty() here, because mocks have 1 class for one mock object, so buttonClass==formClass
        $button = new class extends Block {
        };
        $dd     = $button;
        $form   = new class extends Block {
            public function getDependencies(): array
            {
                global $dd;

                return [$dd];
            }
        };
        $header = $this->makeEmpty(BlockInterface::class, [
            'getDependencies' => function () use ($form) {
                return [$form,];
            }
        ]);

        $renderer->render($header);
        unset($dd);

        $this->assertEmpty($renderer->getDependencyChain());
    }

    public function testRenderAddsBlockDependenciesToUsedListRecursivelyThrowsErrorWhenPermanentRecursion()
    {
        $renderer = $this->getRendererForRenderTest();

        global $dd;
        // don't use makeEmpty() here, because mocks have 1 class for one mock object, so buttonClass==formClass
        $form   = new class extends Block {
            public function getDependencies(): array
            {
                global $dd;

                return [$dd];
            }
        };
        $header = $this->makeEmpty(BlockInterface::class, [
            'getDependencies' => function () use ($form) {
                return [$form,];
            }
        ]);
        $dd     = $header;

        $isWithException = false;
        try {
            $renderer->render($header);
        } catch (\Exception $ex) {
            $isWithException = true;
        }
        unset($dd);

        $this->assertTrue($isWithException);
    }
}
