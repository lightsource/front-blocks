<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Test\Unit;
use LightSource\FrontBlocks\Block\Template;
use Twig\Loader\ArrayLoader;


class TwigTest extends Unit
{
    private function renderBlock(array $blocks, string $template, array $renderArgs = []): string
    {
        $twigLoader = new ArrayLoader($blocks);
        $settings   = new Settings();
        $twig       = new Twig($settings, null, $twigLoader);
        $twig->initialize();

        return $twig->render($template, $renderArgs);
    }

    public function testMergeArgsRecursively()
    {
        $twig = $this->make(Twig::class);
        $this->assertEquals(
            [
                'classes' => [
                    'first',
                    'second',
                ],
                'value'   => 2,
            ],
            $twig->mergeArgsRecursively(
                [
                    'classes' => [
                        'first',
                    ],
                    'value'   => 1,
                ],
                [
                    'classes' => [
                        'second',
                    ],
                    'value'   => 2,
                ]
            )
        );
    }

    public function testExtendTwigIncludeFunctionWhenBlockIsLoaded()
    {
        $blocks     = [
            'form.twig'   => '{{ _include(button) }}',
            'button.twig' => 'button content',
        ];
        $template   = 'form.twig';
        $renderArgs = [
            'button' => [
                Template::KEY_TEMPLATE  => 'button.twig',
                Template::KEY_IS_LOADED => true,
            ],
        ];

        $this->assertEquals('button content', $this->renderBlock($blocks, $template, $renderArgs));
    }

    public function testExtendTwigIncludeFunctionWhenBlockNotLoaded()
    {
        $blocks     = [
            'form.twig'   => '{{ _include(button) }}',
            'button.twig' => 'button content',
        ];
        $template   = 'form.twig';
        $renderArgs = [
            'button' => [
                Template::KEY_TEMPLATE  => 'button.twig',
                Template::KEY_IS_LOADED => false,
            ],
        ];

        $this->assertEquals('', $this->renderBlock($blocks, $template, $renderArgs));
    }

    public function testExtendTwigIncludeFunctionWhenArgsPassed()
    {
        $blocks     = [
            'form.twig'   => '{{ _include(button,{classes:["test-class",],}) }}',
            'button.twig' => '{{ classes|join(" ") }}',
        ];
        $template   = 'form.twig';
        $renderArgs = [
            'button' => [
                Template::KEY_TEMPLATE  => 'button.twig',
                Template::KEY_IS_LOADED => true,
                'classes'               => ['own-class',],
            ],
        ];

        $this->assertEquals('own-class test-class', $this->renderBlock($blocks, $template, $renderArgs));
    }

    public function testExtendTwigMergeFilter()
    {
        $blocks     = [
            'button.twig' => '{{ {"array":["first",],}|_merge({"array":["second",],}).array|join(" ") }}',
        ];
        $template   = 'button.twig';
        $renderArgs = [];

        $this->assertEquals('first second', $this->renderBlock($blocks, $template, $renderArgs));
    }
}
