<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Test\Unit;

class SettingsTest extends Unit
{
    public function testGetFolderByBlockClass()
    {
        $settings = new Settings();
        $settings->addBlocksFolder('TestNamespace', 'test-folder');

        $namespace = '';
        $folder    = $settings->getFolderByBlockClass('TestNamespace\Class', $namespace);

        $this->assertEquals('test-folder', $folder);
        $this->assertEquals('TestNamespace', $namespace);
    }

    public function testGetFolderByBlockClassWhenSeveralFoldersAreAvailable()
    {
        $settings = new Settings();
        $settings->addBlocksFolder('TestNamespace', 'test-folder');
        $settings->addBlocksFolder('SecondNamespace', 'second-folder');

        $namespace = '';
        $folder    = $settings->getFolderByBlockClass('TestNamespace\Class', $namespace);

        $this->assertEquals('test-folder', $folder);
        $this->assertEquals('TestNamespace', $namespace);
    }

    public function testGetFolderByBlockClassIgnoresWrong()
    {
        $settings = new Settings();
        $settings->addBlocksFolder('TestNamespace', 'test-folder');

        $namespace = '';

        $this->assertEquals(
            null,
            $settings->getFolderByBlockClass('WrongNamespace\Class', $namespace)
        );
    }
}
