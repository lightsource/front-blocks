<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use LightSource\FrontBlocks\Block\Block;
use LightSource\FrontBlocks\Interfaces\BlockInterface;
use LightSource\FrontBlocks\Interfaces\BlocksSetuperInterface;
use LightSource\FrontBlocks\Interfaces\CreatorInterface;
use LightSource\FrontBlocks\Interfaces\ExternalDependenciesInterface;
use LightSource\FrontBlocks\Interfaces\SettingsInterface;
use org\bovigo\vfs\vfsStream;
use Psr\Container\ContainerInterface;
use UnitTester;

class LoaderTest extends Unit
{
    protected UnitTester $tester;

    public function testLoadAllBlocks()
    {
        $rootDirectory = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace     = $this->tester->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());
        $blocksFolder  = vfsStream::create(
            [
                'ButtonBase'  => [
                    'ButtonBase.php' => $this->tester->getBlockClassFile(
                        $namespace . '\ButtonBase',
                        'ButtonBase',
                        '\\' . Block::class
                    ),
                ],
                'ButtonChild' => [
                    'ButtonChild.php' => $this->tester->getBlockClassFile(
                        $namespace . '\ButtonChild',
                        'ButtonChild',
                        '\\' . $namespace . '\ButtonBase\ButtonBase'
                    ),
                ],
            ],
            $rootDirectory
        );

        $settings = $this->makeEmpty(SettingsInterface::class, [
            'getBlockFoldersInfo' => function () use (
                $namespace,
                $blocksFolder
            ) {
                return [$namespace => $blocksFolder->url(),];
            },
        ]);

        $loader = new Loader($settings);
        $loader->loadAllBlocks();

        $this->assertEquals(
            [
                $namespace . '\ButtonBase\ButtonBase',
                $namespace . '\ButtonChild\ButtonChild',
            ],
            $loader->getLoadedBlockClasses()
        );
    }

    public function testLoadAllBlocksWhenFilePregIsSet()
    {
        $rootDirectory = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace     = $this->tester->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());
        $blocksFolder  = vfsStream::create(
            [
                'ButtonBase'  => [
                    'ButtonBase.php' => $this->tester->getBlockClassFile(
                        $namespace . '\ButtonBase',
                        'ButtonBase',
                        '\\' . Block::class
                    ),
                ],
                'ButtonData' => [
                    'ButtonData.php' => $this->tester->getBlockClassFile(
                        $namespace . '\ButtonData',
                        'ButtonData',
                        '\\' . Block::class
                    ),
                ],
            ],
            $rootDirectory
        );

        $settings = $this->makeEmpty(SettingsInterface::class, [
            'getBlockFoldersInfo' => function () use (
                $namespace,
                $blocksFolder
            ) {
                return [$namespace => $blocksFolder->url(),];
            },
        ]);

        $loader = new Loader($settings);
        $loader->loadAllBlocks('/(?<!Data).php$/');

        $this->assertEquals(
            [
                $namespace . '\ButtonBase\ButtonBase',
            ],
            $loader->getLoadedBlockClasses()
        );
    }

    public function testLoadAllBlocksCreatesBlockInstanceAndCallsBlockSetupMethod()
    {
        $rootDirectory       = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace           = $this->tester->getUniqueControllerNamespaceWithAutoloader(
            __METHOD__,
            $rootDirectory->url()
        );
        $blocksFolder        = vfsStream::create(
            [
                'ButtonBase' => [
                    'ButtonBase.php' => $this->tester->getBlockClassFile(
                        $namespace . '\ButtonBase',
                        'ButtonBase',
                        '\\' . Block::class
                    ),
                ],
            ],
            $rootDirectory
        );
        $setupCalledForClass = '';

        $blocksSetuper = $this->makeEmpty(BlocksSetuperInterface::class, [
            'setupBlock' => function (string $blockClass) use (&$setupCalledForClass) {
                $setupCalledForClass = $blockClass;
            }
        ]);
        $settings      = $this->makeEmpty(SettingsInterface::class, [
            'getBlockFoldersInfo' => function () use (
                $namespace,
                $blocksFolder
            ) {
                return [$namespace => $blocksFolder->url(),];
            },
        ]);
        $loader        = new Loader($settings);
        $loader->setBlocksSetuper($blocksSetuper);

        $loader->loadAllBlocks();

        $this->assertEquals(
            'LightSource\FrontBlocks\LoaderTest_testLoadAllBlocksCreatesBlockInstanceAndCallsBlockSetupMethod\ButtonBase\ButtonBase',
            $setupCalledForClass
        );
    }

    public function testLoadAllBlocksIgnoresPhpFilesWithoutBlocks()
    {
        $rootDirectory = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace     = $this->tester->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());
        $blocksFolder  = vfsStream::create(
            [
                'ButtonBase'  => [
                    'ButtonBase.php' => $this->tester->getBlockClassFile(
                        $namespace . '\ButtonBase',
                        'ButtonBase',
                        '\\' . Block::class
                    ),
                ],
                'ButtonChild' => [
                    'ButtonChild.php' => '<?php namespace ' . $namespace . '; class ButtonChild{}',
                ],
            ],
            $rootDirectory
        );

        $settings = $this->makeEmpty(SettingsInterface::class, [
            'getBlockFoldersInfo' => function () use (
                $namespace,
                $blocksFolder
            ) {
                return [$namespace => $blocksFolder->url(),];
            },
        ]);

        $loader = new Loader($settings);
        $loader->loadAllBlocks();

        $this->assertEquals(
            [
                $namespace . '\ButtonBase\ButtonBase',
            ],
            $loader->getLoadedBlockClasses()
        );
    }

    public function testLoadAllBlocksInSeveralFolders()
    {
        $rootDirectory   = $this->tester->getUniqueDirectory(__METHOD__);
        $firstFolderUrl  = $rootDirectory->url() . '/First';
        $secondFolderUrl = $rootDirectory->url() . '/Second';
        $firstNamespace  = $this->tester->getUniqueControllerNamespaceWithAutoloader(
            __METHOD__ . '_first',
            $firstFolderUrl,
        );
        $secondNamespace = $this->tester->getUniqueControllerNamespaceWithAutoloader(
            __METHOD__ . '_second',
            $secondFolderUrl,
        );
        vfsStream::create(
            [
                'First'  => [
                    'ButtonBase' => [
                        'ButtonBase.php' => $this->tester->getBlockClassFile(
                            $firstNamespace . '\ButtonBase',
                            'ButtonBase',
                            '\\' . Block::class
                        ),
                    ],
                ],
                'Second' => [
                    'ButtonBase' => [
                        'ButtonBase.php' => $this->tester->getBlockClassFile(
                            $secondNamespace . '\ButtonBase',
                            'ButtonBase',
                            '\\' . Block::class
                        ),
                    ],
                ],
            ],
            $rootDirectory
        );

        $settings = new Settings();
        $settings->addBlocksFolder($firstNamespace, $firstFolderUrl);
        $settings->addBlocksFolder($secondNamespace, $secondFolderUrl);

        $settings = $this->makeEmpty(SettingsInterface::class, [
            'getBlockFoldersInfo' => function () use (
                $firstNamespace,
                $firstFolderUrl,
                $secondNamespace,
                $secondFolderUrl
            ) {
                return [$firstNamespace => $firstFolderUrl, $secondNamespace => $secondFolderUrl,];
            },
        ]);

        $loader = new Loader($settings);
        $loader->loadAllBlocks();

        $this->assertEquals(
            [
                $firstNamespace . '\ButtonBase\ButtonBase',
                $secondNamespace . '\ButtonBase\ButtonBase',
            ],
            $loader->getLoadedBlockClasses()
        );
    }

    public function testLoadAllBlocksFor1K()
    {
        return;
        $rootDirectory = $this->tester->getUniqueDirectory(__METHOD__);
        $namespace     = $this->tester->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory->url());

        $fileStructure = [];
        for ($i = 0; $i < 1000; $i++) {
            $fileStructure['ButtonBase' . $i]  = [
                'ButtonBase' . $i . '.php' => $this->tester->getBlockClassFile(
                    $namespace . '\ButtonBase' . $i,
                    'ButtonBase' . $i,
                    '\\' . Block::class
                ),
            ];
            $fileStructure['ButtonChild' . $i] = [
                'ButtonChild' . $i . '.php' => $this->tester->getBlockClassFile(
                    $namespace . '\ButtonChild' . $i,
                    'ButtonChild' . $i,
                    '\\' . $namespace . '\ButtonBase' . $i . '\ButtonBase' . $i
                ),
            ];
        }

        $blocksFolder = vfsStream::create($fileStructure, $rootDirectory);

        $creator  = $this->makeEmpty(CreatorInterface::class, [
            'create' => function (string $blockClass): ?BlockInterface {
                return $this->makeEmpty(BlockInterface::class);
            }
        ]);
        $settings = $this->makeEmpty(SettingsInterface::class, [
            'getBlockFoldersInfo' => function () use (
                $namespace,
                $blocksFolder
            ) {
                return [$namespace => $blocksFolder->url(),];
            },
        ]);

        $loader = new Loader($creator, $settings);

        $loader->loadAllBlocks();

        $this->assertCount(2000, $loader->getLoadedBlockClasses());
        // codecept_debug($loader->getLoadTimeSec());//fixme
        // $this->assertLessThan(1, $loader->getLoadTimeSec());
    }
}
