<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Test\Unit;
use LightSource\FrontBlocks\Block\ResourceCreator;
use org\bovigo\vfs\vfsStream;

class BlockDuplicatorTest extends Unit
{
    // get a unique directory name depending on a test method to prevent affect other tests
    private function getUniqueDirName(string $methodConstant): string
    {
        return str_replace([':', '\\'], '_', $methodConstant);
    }

    private function testCopy(
        string $methodConstant,
        array $fileStructure,
        string $sourceBlockFile,
        string $targetBlockFile,
        bool $isWithReplace
    ): string {
        $rootDirName = $this->getUniqueDirName($methodConstant);
        $directory   = vfsStream::setup($rootDirName);
        vfsStream::create($fileStructure, $directory);
        $directoryUrl = $directory->url();

        $blockDuplicator = new Duplicator($directoryUrl, $sourceBlockFile, $targetBlockFile, new ResourceCreator(null));
        $blockDuplicator->copy($isWithReplace);

        return $directoryUrl;
    }

    public function testCopyFiles()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Source' => [
                    'Source.php'  => '',
                    'Source.twig' => '',
                ],
            ],
            'Source/Source.php',
            'Target/Source.php',
            false
        );

        $targetTwigFile  = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Source',
                'Source.twig',
            ]
        );
        $targetBlockFile = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Target',
                'Source.php',
            ]
        );

        $this->assertTrue(is_file($targetBlockFile) && is_file($targetTwigFile));
    }

    public function testCopyIgnoresFolders()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Source' => [
                    'Source.php'  => '',
                    'EmptyFolder' => [],
                ],
            ],
            'Source/Source.php',
            'Target/Source.php',
            false
        );

        $targetEmptyFolder = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Target',
                'EmptyFolder',
            ]
        );

        $this->assertFalse(is_dir($targetEmptyFolder));
    }

    public function testCopyIgnoresWhenTargetFolderAlreadyExists()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Source' => [
                    'Source.php'  => '',
                    'Source.twig' => '',
                ],
                'Target' => [

                ],
            ],
            'Source/Source.php',
            'Target/Source.php',
            false
        );

        $targetTwigFile = is_file(
            implode(
                DIRECTORY_SEPARATOR,
                [
                    $directoryUrl,
                    'Target',
                    'Source.twig',
                ]
            )
        );

        $this->assertFalse($targetTwigFile);
    }

    public function testCopyWhenTargetContainsSeveralNewFolders()
    {
        $directoryUrl   = $this->testCopy(
            __METHOD__,
            [
                'Source' => [
                    'Source.php'  => '',
                    'Source.twig' => '',
                ],
            ],
            'Source/Source.php',
            'Target/Theme/Main/Source.php',
            false
        );
        $targetTwigFile = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Target',
                'Theme',
                'Main',
                'Source.twig',
            ]
        );

        $this->assertTrue(is_file($targetTwigFile));
    }

    public function testCopyReplacesInFileName()
    {
        $directoryUrl   = $this->testCopy(
            __METHOD__,
            [
                'Source' => [
                    'Source.php'  => '',
                    'Source.twig' => '',
                ],
            ],
            'Source/Source.php',
            'Target/Target.php',
            true
        );
        $targetTwigFile = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Target',
                'Target.twig',
            ]
        );

        $this->assertTrue(is_file($targetTwigFile));
    }

    public function testCopyIgnoresReplaceInWrongFileName()
    {
        $directoryUrl   = $this->testCopy(
            __METHOD__,
            [
                'Source' => [
                    'Source.php'   => '',
                    'Another.twig' => '',
                ],
            ],
            'Source/Source.php',
            'Target/Target.php',
            true
        );
        $targetTwigFile = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Target',
                'Source.twig',
            ]
        );

        $this->assertFalse(is_file($targetTwigFile));
    }

    public function testCopyReplacesInFileContent()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Source' => [
                    'Source.php'  => '',
                    'Source.twig' => '<div class="Source"></div>',
                ],
            ],
            'Source/Source.php',
            'Target/Target.php',
            true
        );

        $targetTwigFile    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Target',
                'Target.twig',
            ]
        );
        $targetTwigContent = is_file($targetTwigFile) ?
            file_get_contents($targetTwigFile) :
            '';

        $this->assertEquals('<div class="Target"></div>', $targetTwigContent);
    }

    public function testCopyReplacesInFileContentWithLowerCase()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Source' => [
                    'Source.php'  => '',
                    'Source.twig' => '<div class="source"></div>',
                ],
            ],
            'Source/Source.php',
            'Target/Target.php',
            true
        );

        $targetTwigFile    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Target',
                'Target.twig',
            ]
        );
        $targetTwigContent = is_file($targetTwigFile) ?
            file_get_contents($targetTwigFile) :
            '';

        $this->assertEquals('<div class="target"></div>', $targetTwigContent);
    }
}
