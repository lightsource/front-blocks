<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Test\Unit;
use LightSource\FrontBlocks\Block\Creator;
use LightSource\FrontBlocks\Interfaces\BlockInterface;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use Exception;

class CreatorTest extends Unit
{
    private function getBlockWithTestProperty(): BlockInterface
    {
        return new class (null) implements BlockInterface {

            protected $test;

            public function __construct($test)
            {
                $this->test = $test;
            }

            public static function setup(?ContainerInterface $container): void
            {
            }

            public function isLoaded(): bool
            {
                return false;
            }

            public function getFieldsInfo(): array
            {
                return ['test' => '',];
            }

            public function getDependencies(): array
            {
                return [];
            }

            public function getTemplateArgs(): array
            {
                return [];
            }

            public function getTest()
            {
                return $this->test;
            }

            public function getDeepClone(): BlockInterface
            {
                return $this;
            }
        };
    }

    public function testCreate()
    {
        $block      = $this->makeEmpty(BlockInterface::class);
        $blockClass = get_class($block);
        $creator    = $this->make(Creator::class, [
            'creationChain' => [],
        ]);

        $this->assertEquals($blockClass, get_class($creator->create($blockClass)));
    }

    public function testCreateWithExtraDependencyInConstructor()
    {
        $dependency = new class {
        };
        $block      = $this->getBlockWithTestProperty();

        $creator       = $this->make(Creator::class, [
            'container'    => $this->makeEmpty(ContainerInterface::class, [
                'get' => function (string $class) use ($dependency) {
                    return $class === get_class($dependency) ?
                        $dependency :
                        null;
                }
            ]),
            'creationChain'           => [],
            'getConstructorArguments' => function (ReflectionClass $classInfo) use ($dependency, $block) {
                return get_class($block) === $classInfo->getName() ?
                    [get_class($dependency)] :
                    [];
            }
        ]);
        $blockInstance = $creator->create(get_class($block));

        $this->assertTrue($dependency === $blockInstance->getTest());
    }

    public function testCreateInitializeInnerDependency()
    {
        $dependency = $this->makeEmpty(BlockInterface::class);
        eval(
            '$block = new class extends \LightSource\FrontBlocks\Block\Block{
protected $test; 
public function getTest(){return $this->test;}
public function getFieldsInfo() : array{return ["test"=>"' . get_class($dependency) . '",];}
};'
        );
        $block = $block ?? null;

        $creator       = $this->make(Creator::class, [
            'creationChain' => [],
        ]);
        $blockInstance = $creator->create(get_class($block));

        $this->assertEquals(get_class($dependency), get_class($blockInstance->getTest()));
    }

    public function testCreateInitializeInnerDependencySupportsPolymorphism()
    {
        $parent = $this->makeEmpty(BlockInterface::class);
        global $dd;
        eval(
            '$child = new class extends \\' . get_class($parent) . ' {};'
        );
        $child = $child ?? null;
        $dd    = get_class($child);
        eval(
            '$block = new class extends \LightSource\FrontBlocks\Block\Block{
protected $test; 
public function __construct(){parent::__construct();global $dd;$this->test=new $dd();}
public function getTest(){return $this->test;}
public function getFieldsInfo():array{return ["test"=>"' . get_class($parent) . '",];}
};'
        );
        $block = $block ?? null;

        $creator       = $this->make(Creator::class, [
            'creationChain' => [],
        ]);
        $blockInstance = $creator->create(get_class($block));

        $this->assertEquals(get_class($child), get_class($blockInstance->getTest()));
    }

    public function testCreateInitializeInnerDependencyWithExtraDependencyInConstructor()
    {
        $extra = new class {
        };
        eval(
        '$dependency = new class (null) extends \LightSource\FrontBlocks\Block\Block{
private $extra;
public function __construct($extra){parent::__construct();$this->extra=$extra;}
public function getExtra(){return $this->extra;}
};'
        );
        $dependency = $dependency ?? null;
        eval(
            '$block = new class extends \LightSource\FrontBlocks\Block\Block{
protected $test; 
public function getFieldsInfo() : array{return ["test"=>"' . get_class($dependency) . '"];}
public function getTest(){return $this->test;}
};'
        );
        $block = $block ?? null;

        $creator       = $this->make(Creator::class, [
            'creationChain'           => [],
            'container'    => $this->makeEmpty(ContainerInterface::class, [
                'get' => function ($class) use ($extra) {
                    if (get_class($extra) === $class) {
                        return $extra;
                    }

                    return null;
                },
            ]),
            'getConstructorArguments' => function (ReflectionClass $classInfo) use ($dependency, $extra) {
                if (get_class($dependency) === $classInfo->getName()) {
                    return [get_class($extra)];
                }

                return [];
            }
        ]);
        $blockInstance = $creator->create(get_class($block));

        $this->assertTrue($extra === $blockInstance->getTest()->getExtra());
    }

    public function testCreateInitializeInnerDependencyRecursively()
    {
        $first = $this->makeEmpty(BlockInterface::class);
        eval(
            '$dependency = new class (null) extends \LightSource\FrontBlocks\Block\Block{
protected $test; 
public function getFieldsInfo() : array{return ["test"=>"' . get_class($first) . '"];}
public function getTest(){return $this->test;}
};'
        );
        $dependency = $dependency ?? null;
        eval(
            '$block = new class extends \LightSource\FrontBlocks\Block\Block{
protected $test; 
public function getFieldsInfo() : array{return ["test"=>"' . get_class($dependency) . '"];}
public function getTest(){return $this->test;}
};'
        );
        $block = $block ?? null;

        $creator       = $this->make(Creator::class, [
            'creationChain' => [],
        ]);
        $blockInstance = $creator->create(get_class($block));

        $this->assertEquals(get_class($first), get_class($blockInstance->getTest()->getTest()));
    }

    public function testCreateHasClearChainAfterInitializeInnerDependencyRecursively()
    {
        $first = $this->makeEmpty(BlockInterface::class);
        eval(
            '$dependency = new class (null) extends \LightSource\FrontBlocks\Block\Block{
protected $test; 
public function getFieldsInfo() : array{return ["test"=>"' . get_class($first) . '"];}
public function getTest(){return $this->test;}
};'
        );
        $dependency = $dependency ?? null;
        eval(
            '$block = new class extends \LightSource\FrontBlocks\Block\Block{
protected $test; 
public function getFieldsInfo() : array{return ["test"=>"' . get_class($dependency) . '"];}
public function getTest(){return $this->test;}
};'
        );
        $block = $block ?? null;

        $creator = $this->make(Creator::class, [
            'creationChain' => [],
        ]);
        $creator->create(get_class($block));

        $this->assertEmpty($creator->getCreationChain());
    }

    public function testCreateThrowsExceptionWhenRecursionIsPermanentDuringInitializeInnerDependencyRecursively()
    {
        global $dd;
        eval(
        '$dependency = new class extends \LightSource\FrontBlocks\Block\Block{
protected $test; 
public function getFieldsInfo() : array{global $dd;if(!$dd){return [];}return ["test"=>get_class($dd),];}
public function getTest(){return $this->test;}
};'
        );
        $dependency = $dependency ?? null;
        eval(
            '$block = new class extends \LightSource\FrontBlocks\Block\Block{
protected $dependency; 
public function getFieldsInfo() : array{return ["dependency"=>"' . get_class($dependency) . '"];}
};'
        );
        $block   = $block ?? null;
        $dd      = $block;
        $creator = $this->make(Creator::class, [
            'creationChain' => [],
        ]);

        $isWithException = false;
        try {
            $creator->create(get_class($block));
        } catch (Exception $ex) {
            $isWithException = true;
        }
        unset($dd);

        $this->assertTrue($isWithException);
    }
}
