<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Test\Unit;
use LightSource\FrontBlocks\Block\Block;
use UnitTester;

class BlockTest extends Unit
{
    protected UnitTester $tester;

    public function testReadProtectedFields()
    {
        $block = new class extends Block {
            protected $loadedField;
        };

        $this->assertEquals(['loadedField',], array_keys($block->getFieldsInfo()));
    }

    public function testIgnoreReadPublicFields()
    {
        $block = new class extends Block {
            public $ignoredField;
        };

        $this->assertEmpty($block->getFieldsInfo());
    }

    public function testReadFieldWithType()
    {
        $block = new class extends Block {
            protected string $loadedField;
        };

        $this->assertEquals(['loadedField' => 'string',], $block->getFieldsInfo());
    }

    public function testReadFieldWithoutType()
    {
        $block = new class extends Block {
            protected $loadedField;
        };

        $this->assertEquals(['loadedField' => '',], $block->getFieldsInfo());
    }

    public function testAutoInitIntField()
    {
        $block = new class extends Block {

            protected int $int;

            public function getInt()
            {
                return $this->int;
            }
        };

        $this->assertTrue(0 === $block->getInt());
    }

    public function testAutoInitFloatField()
    {
        $block = new class extends Block {

            protected float $float;

            public function getFloat()
            {
                return $this->float;
            }
        };

        $this->assertTrue(0.0 === $block->getFloat());
    }

    public function testAutoInitStringField()
    {
        $block = new class extends Block {

            protected string $string;

            public function getString()
            {
                return $this->string;
            }
        };

        $this->assertTrue('' === $block->getString());
    }

    public function testAutoInitBoolField()
    {
        $block = new class extends Block {

            protected bool $bool;

            public function getBool()
            {
                return $this->bool;
            }
        };

        $this->assertTrue(false === $block->getBool());
    }

    public function testAutoInitArrayField()
    {
        $block = new class extends Block {

            protected array $array;

            public function getArray()
            {
                return $this->array;
            }
        };

        $this->assertTrue([] === $block->getArray());
    }

    public function testGetDeepCloneRecursively()
    {
        $first = new class extends Block {
            public int $field = 1;
        };
        global $dd;
        $dd     = get_class($first);
        $second = new class extends Block {
            public Block $first;

            public function __construct()
            {
                parent::__construct();
                global $dd;
                $this->first = new $dd();
            }

            public function getFieldsInfo(): array
            {
                return ['first' => ''];
            }
        };
        unset($dd);

        $clone                = $second->getDeepClone();
        $second->first->field = 2;

        $this->assertEquals(2, $second->first->field);
        $this->assertEquals(1, $clone->first->field);
    }
}
