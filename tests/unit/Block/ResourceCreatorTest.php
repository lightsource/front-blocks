<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Test\Unit;
use LightSource\FrontBlocks\Block\ResourceCreator;
use LightSource\FrontBlocks\Interfaces\SettingsInterface;

class ResourceCreatorTest extends Unit
{
    public function testCreateResource()
    {
        $settings        = $this->makeEmpty(SettingsInterface::class, [
            'getFolderByBlockClass' => function (string $blockClass, string &$namespace): ?string {
                $namespace = 'my-namespace';

                return 'my-folder';
            },
        ]);
        $resourceCreator = $this->make(ResourceCreator::class, [
            'settings' => $settings,
            'isBlock'  => function () {
                return true;
            },
        ]);
        $resource        = $resourceCreator->createResource('my-namespace\\Button\\Theme\\Red\\ButtonThemeRed');

        $this->assertEquals('my-namespace', $resource->getNamespace());
        $this->assertEquals('my-folder', $resource->getFolder());
        $this->assertEquals('Button/Theme/Red', $resource->getRelativeFolderPath());
        $this->assertEquals('Button/Theme/Red/ButtonThemeRed', $resource->getRelativePath());
        $this->assertEquals('ButtonThemeRed', $resource->getName());
    }
}
