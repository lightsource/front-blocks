<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Codeception\Test\Unit;
use LightSource\FrontBlocks\Block\Template;
use LightSource\FrontBlocks\Interfaces\BlockInterface;
use LightSource\FrontBlocks\Interfaces\ResourceInterface;
use LightSource\FrontBlocks\Interfaces\ResourceCreatorInterface;
use LightSource\FrontBlocks\Interfaces\SettingsInterface;
use LightSource\FrontBlocks\Interfaces\TwigInterface;
use LightSource\FrontBlocks\Block\Block;
use Exception;

class TemplateTest extends Unit
{
    public function testGetArgsResponseIsEmptyWhenTwigTemplateIsMissing()
    {
        $resource        = $this->makeEmpty(ResourceInterface::class);
        $resourceCreator = $this->makeEmpty(ResourceCreatorInterface::class, [
            'createResource' => function () use ($resource) {
                return $resource;
            }
        ]);
        $twig            = $this->makeEmpty(TwigInterface::class, [
            'clearNamespace' => function ($namespace) {
                return $namespace;
            },
        ]);
        $settings        = $this->makeEmpty(SettingsInterface::class, [
            'getTwigExtension' => function () {
                return '.twig';
            },
        ]);
        $template        = $this->make(Template::class, [
            'resourceCreator' => $resourceCreator,
            'twig'            => $twig,
            'settings'        => $settings,
            'isFileExist'     => function () use (&$counter) {
                return false;
            }
        ]);
        $block           = $this->makeEmpty(BlockInterface::class);

        $args = $template->getArgs($block);

        $this->assertEmpty($args);
    }

    public function testGetArgsIsLoadedConstantIsTrueWhenBlockIsLoaded()
    {
        $resource = $this->makeEmpty(ResourceInterface::class);
        $template = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);
        $block    = $this->makeEmpty(BlockInterface::class, [
            'isLoaded' => function () {
                return true;
            }
        ]);

        $args = $template->getArgs($block);

        $this->assertTrue($args[Template::KEY_IS_LOADED]);
    }

    public function testGetArgsIsLoadedConstantIsFalseWhenBlockIsNotLoaded()
    {
        $resource = $this->makeEmpty(ResourceInterface::class);
        $template = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);
        $block    = $this->makeEmpty(BlockInterface::class, [
            'isLoaded' => function () {
                return false;
            }
        ]);

        $args = $template->getArgs($block);

        $this->assertFalse($args[Template::KEY_IS_LOADED]);
    }

    public function testGetArgsTemplateConstant()
    {
        $resource        = $this->makeEmpty(ResourceInterface::class, [
            'getNamespace'    => function () {
                return 'namespace';
            },
            'getRelativePath' => function () {
                return 'Button/Theme/Red/ButtonThemeRed';
            },
        ]);
        $resourceCreator = $this->makeEmpty(ResourceCreatorInterface::class, [
            'createResource' => function () use ($resource) {
                return $resource;
            }
        ]);
        $twig            = $this->makeEmpty(TwigInterface::class, [
            'clearNamespace' => function ($namespace) {
                return $namespace;
            },
        ]);
        $settings        = $this->makeEmpty(SettingsInterface::class, [
            'getTwigExtension' => function () {
                return '.twig';
            },
        ]);
        $template        = $this->make(Template::class, [
            'resourceCreator' => $resourceCreator,
            'twig'            => $twig,
            'settings'        => $settings,
            'isFileExist'     => function () {
                return true;
            }
        ]);
        $block           = $this->makeEmpty(BlockInterface::class);

        $args = $template->getArgs($block);

        $this->assertEquals('@namespace/Button/Theme/Red/ButtonThemeRed.twig', $args[Template::KEY_TEMPLATE]);
    }

    public function testGetArgsTemplateConstantWhenTwigTemplateAtParent()
    {
        $counter         = 0;
        $resource        = $this->makeEmpty(ResourceInterface::class, [
            'getNamespace'    => function () {
                return 'namespace';
            },
            'getRelativePath' => function () use (&$counter) {
                if (1 === $counter) {
                    return 'Button/Theme/Red/ButtonThemeRed';
                }

                return 'Button/Button';
            },
        ]);
        $resourceCreator = $this->makeEmpty(ResourceCreatorInterface::class, [
            'createResource' => function () use ($resource) {
                return $resource;
            }
        ]);
        $twig            = $this->makeEmpty(TwigInterface::class, [
            'clearNamespace' => function ($namespace) {
                return $namespace;
            },
        ]);
        $settings        = $this->makeEmpty(SettingsInterface::class, [
            'getTwigExtension' => function () {
                return '.twig';
            },
        ]);
        $template        = $this->make(Template::class, [
            'resourceCreator' => $resourceCreator,
            'twig'            => $twig,
            'settings'        => $settings,
            'getParentClass'  => function () use (&$counter) {
                if (1 === $counter) {
                    return 'parentClass';
                }

                return '';
            },
            'isFileExist'     => function () use (&$counter) {
                $counter++;

                return 2 === $counter;
            }
        ]);
        $block           = $this->makeEmpty(BlockInterface::class);

        $args = $template->getArgs($block);

        $this->assertEquals('@namespace/Button/Button.twig', $args[Template::KEY_TEMPLATE]);
    }

    public function testGetArgsParentConstantIsEmptyWhenThereIsNoParentWithTwigTemplate()
    {
        $counter         = 0;
        $resource        = $this->makeEmpty(ResourceInterface::class, [
            'getNamespace'    => function () {
                return 'namespace';
            },
            'getRelativePath' => function () use (&$counter) {
                if (0 === $counter) {
                    return 'Button/Theme/Red/ButtonThemeRed';
                }

                return 'Button/Button';
            },
        ]);
        $resourceCreator = $this->makeEmpty(ResourceCreatorInterface::class, [
            'createResource' => function () use ($resource) {
                return $resource;
            }
        ]);
        $twig            = $this->makeEmpty(TwigInterface::class, [
            'clearNamespace' => function ($namespace) {
                return $namespace;
            },
        ]);
        $settings        = $this->makeEmpty(SettingsInterface::class, [
            'getTwigExtension'      => function () {
                return '.twig';
            },
            'getFolderByBlockClass' => function (string $blockClass, string &$namespace): ?string {
                return 'folder';
            },
        ]);
        $template        = $this->make(Template::class, [
            'resourceCreator' => $resourceCreator,
            'twig'            => $twig,
            'settings'        => $settings,
            'getParentClass'  => function () use (&$counter) {
                $counter++;

                if (1 === $counter) {
                    return 'parentClass';
                }

                return '';
            },
            'isFileExist'     => function () use (&$counter) {
                return 0 === $counter;
            }
        ]);
        $block           = $this->makeEmpty(BlockInterface::class);

        $args = $template->getArgs($block);

        $this->assertEmpty($args[Template::KEY_PARENT]);
    }

    public function testGetArgsParentConstantIsFilledWhenThereIsParentWithTwigTemplate()
    {
        $counter         = 0;
        $resource        = $this->makeEmpty(ResourceInterface::class, [
            'getNamespace'    => function () {
                return 'namespace';
            },
            'getRelativePath' => function () use (&$counter) {
                if (2 === $counter) {
                    return 'Button/Button';
                }

                return 'Button/Theme/Red/ButtonThemeRed';
            },
        ]);
        $resourceCreator = $this->makeEmpty(ResourceCreatorInterface::class, [
            'createResource' => function () use ($resource) {
                return $resource;
            }
        ]);
        $twig            = $this->makeEmpty(TwigInterface::class, [
            'clearNamespace' => function ($namespace) {
                return $namespace;
            },
        ]);
        $settings        = $this->makeEmpty(SettingsInterface::class, [
            'getTwigExtension'      => function () {
                return '.twig';
            },
            'getFolderByBlockClass' => function (string $blockClass, string &$namespace): ?string {
                return 'folder';
            },
        ]);
        $template        = $this->make(Template::class, [
            'resourceCreator' => $resourceCreator,
            'twig'            => $twig,
            'settings'        => $settings,
            'getParentClass'  => function () {
                return 'parentClass';
            },
            'isFileExist'     => function () use (&$counter) {
                $counter++;

                return true;
            },
        ]);
        $block           = $this->makeEmpty(BlockInterface::class);

        $args = $template->getArgs($block);

        $this->assertEquals('@namespace/Button/Button.twig', $args[Template::KEY_PARENT]);
    }

    public function testGetArgsContainsBlockTemplateArgs()
    {
        $resource = $this->makeEmpty(ResourceInterface::class);
        $template = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);
        $block    = $this->makeEmpty(BlockInterface::class, [
            'getTemplateArgs' => function () {
                return ['fieldName' => 'fieldValue',];
            },
        ]);

        $args = $template->getArgs($block);

        $this->assertEquals('fieldValue', $args['fieldName']);
    }

    public function testGetArgsCallsGetArgsForInnerBlocksRecursively()
    {
        $resource = $this->makeEmpty(ResourceInterface::class);
        $template = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);
        global $d1, $d2;
        $dependency  = new class extends Block {
            public function getTemplateArgs(): array
            {
                return ['field' => 'fieldValue',];
            }

            public function isLoaded(): bool
            {
                return true;
            }
        };
        $d1          = $dependency;
        $firstBlock  = new class extends Block {
            public function getTemplateArgs(): array
            {
                global $d1;

                return ['button' => $d1];
            }
        };
        $d2          = $firstBlock;
        $secondBlock = new class extends Block {
            public function getTemplateArgs(): array
            {
                global $d2;

                return ['header' => $d2];
            }
        };

        $args = $template->getArgs($secondBlock);

        unset($d1, $d2);

        $this->assertTrue($args['header']['button']['_isLoaded']);
        $this->assertEquals('fieldValue', $args['header']['button']['field']);
    }

    public function testGetArgsHasClearChainAfterCallsGetArgsForInnerBlocksRecursively()
    {
        $resource = $this->makeEmpty(ResourceInterface::class);
        $template = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);

        global $d1, $d2;
        $dependency  = new class extends Block {
            public function getTemplateArgs(): array
            {
                return ['field' => 'fieldValue',];
            }

            public function isLoaded(): bool
            {
                return true;
            }
        };
        $d1          = $dependency;
        $firstBlock  = new class extends Block {
            public function getTemplateArgs(): array
            {
                global $d1;

                return ['button' => $d1];
            }
        };
        $d2          = $firstBlock;
        $secondBlock = new class extends Block {
            public function getTemplateArgs(): array
            {
                global $d2;

                return ['header' => $d2];
            }
        };

        $template->getArgs($secondBlock);
        unset($d1, $d2);

        $this->assertEmpty($template->getArgsChain());
    }

    public function testGetArgsThrowsExceptionWhenRecursionIsPermanentDuringCallsForInnerBlocks()
    {
        $resource    = $this->makeEmpty(ResourceInterface::class);
        $template    = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);
        $secondBlock = null;
        $firstBlock  = $this->makeEmpty(BlockInterface::class, [
            'getTemplateArgs' => function () use (&$secondBlock) {
                return ['button' => $secondBlock];
            },
        ]);
        $secondBlock = $this->makeEmpty(BlockInterface::class, [
            'getTemplateArgs' => function () use ($firstBlock) {
                return ['header' => $firstBlock];
            },
        ]);

        $isWithException = false;
        try {
            $template->getArgs($secondBlock);
        } catch (Exception $ex) {
            $isWithException = true;
        }

        $this->assertTrue($isWithException);
    }

    public function testGetArgsCallsGetArgsForInnerBlocksInArrayFieldRecursively()
    {
        $resource = $this->makeEmpty(ResourceInterface::class);
        $template = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);
        global $d1, $d2;
        $dependency  = new class extends Block {
            public function getTemplateArgs(): array
            {
                return ['field' => 'fieldValue',];
            }

            public function isLoaded(): bool
            {
                return true;
            }
        };
        $d1          = $dependency;
        $firstBlock  = new class extends Block {
            public function getTemplateArgs(): array
            {
                global $d1;

                return ['buttons' => [$d1,],];
            }
        };
        $d2          = $firstBlock;
        $secondBlock = new class extends Block {
            public function getTemplateArgs(): array
            {
                global $d2;

                return ['headers' => [$d2,],];
            }
        };

        $args = $template->getArgs($secondBlock);
        unset($d1, $d2);

        $this->assertTrue($args['headers'][0]['buttons'][0]['_isLoaded']);
        $this->assertEquals('fieldValue', $args['headers'][0]['buttons'][0]['field']);
    }

    public function testGetArgsHasClearChainAfterCallsGetArgsForInnerBlocksInArrayFieldRecursively()
    {
        $resource = $this->makeEmpty(ResourceInterface::class);
        $template = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);
        global $d1, $d2;
        $dependency  = new class extends Block {
            public function getTemplateArgs(): array
            {
                return ['field' => 'fieldValue',];
            }

            public function isLoaded(): bool
            {
                return true;
            }
        };
        $d1          = $dependency;
        $firstBlock  = new class extends Block {
            public function getTemplateArgs(): array
            {
                global $d1;

                return ['buttons' => [$d1,],];
            }
        };
        $d2          = $firstBlock;
        $secondBlock = new class extends Block {
            public function getTemplateArgs(): array
            {
                global $d2;

                return ['headers' => [$d2,],];
            }
        };

        $template->getArgs($secondBlock);
        unset($d1, $d2);

        $this->assertEmpty($template->getArgsChain());
    }

    public function testGetArgsThrowsExceptionWhenRecursionIsPermanentDuringCallsForInnerBlocksInArrayField()
    {
        $resource    = $this->makeEmpty(ResourceInterface::class);
        $template    = $this->make(Template::class, [
            'getResourceRecursivelyWhereTwigExist' => function () use ($resource) {
                return $resource;
            },
            'getTwigRelativeTemplatePath'          => function () {
                return '';
            },
            'twig'                                 => $this->makeEmpty(TwigInterface::class),
        ]);
        $secondBlock = null;
        $firstBlock  = $this->makeEmpty(BlockInterface::class, [
            'getTemplateArgs' => function () use (&$secondBlock) {
                return ['buttons' => [$secondBlock,],];
            },
        ]);
        $secondBlock = $this->makeEmpty(BlockInterface::class, [
            'getTemplateArgs' => function () use ($firstBlock) {
                return ['headers' => [$firstBlock,],];
            },
        ]);

        $isWithException = false;
        try {
            $template->getArgs($secondBlock);
        } catch (Exception $ex) {
            $isWithException = true;
        }

        $this->assertTrue($isWithException);
    }
}
