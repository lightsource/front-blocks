<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

interface ResourceInterface
{
    public function setNamespace(string $namespace): void;

    public function setFolder(string $folder): void;

    public function setRelativeFolderPath(string $relativeFolderPath): void;

    public function setRelativePath(string $relativePath): void;

    public function setName(string $name): void;

    public function getNamespace(): string;

    public function getFolder(): string;

    public function getRelativeFolderPath(): string;

    public function getRelativePath(): string;

    public function getName(): string;
}
