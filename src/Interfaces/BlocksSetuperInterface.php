<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

use Psr\Container\ContainerInterface;

interface BlocksSetuperInterface
{
    public function setupBlock(string $blockClass, ?ContainerInterface $container): void;
}
