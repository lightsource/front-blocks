<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

use Exception;

interface ResourceCreatorInterface
{
    /**
     * @throws Exception
     */
    public function createResource(string $blockClass): ResourceInterface;
}
