<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

use Psr\Container\ContainerInterface;

interface BlockInterface
{
    public static function setup(?ContainerInterface $container): void;

    public function isLoaded(): bool;

    public function getFieldsInfo(): array;

    public function getDependencies(): array;

    public function getTemplateArgs(): array;

    /**
     * @return static
     */
    public function getDeepClone(): self;
}
