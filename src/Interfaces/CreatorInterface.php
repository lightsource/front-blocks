<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

use Exception;

interface CreatorInterface
{
    /**
     * @template T
     *
     * @param class-string<T> $blockClass
     *
     * @return T
     * @throws Exception
     */
    public function create(string $blockClass): BlockInterface;
}
