<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

use Exception;

interface TemplateInterface
{
    /**
     * @throws Exception
     */
    public function getArgs(BlockInterface $block): array;
}
