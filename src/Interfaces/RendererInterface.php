<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

use Exception;

interface RendererInterface
{
    public function setRenderContent(RenderContentInterface $renderContent): void;

    /**
     * @throws Exception
     */
    public function getUsedResources(
        string $extension,
        bool $isIncludeSource = false,
        array $excludeBlocks = []
    ): string;

    /**
     * @throws Exception
     */
    public function render(BlockInterface $block, array $args = [], bool $isPrint = false): string;
}
