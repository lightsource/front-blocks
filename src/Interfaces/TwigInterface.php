<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

use Exception;
use Twig\Environment;

interface TwigInterface
{
    public function mergeArgsRecursively(array $args1, array $args2): array;

    public function clearNamespace(string $namespace): string;

    public function getEnvironment(): ?Environment;

    /**
     * @throws Exception
     */
    public function initialize(): void;

    public function render(string $templateWithNamespace, array $args = [], bool $isPrint = false): string;
}
