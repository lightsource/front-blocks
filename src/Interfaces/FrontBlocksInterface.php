<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

use LightSource\FrontBlocks\Block\ResourceCreator;
use LightSource\FrontBlocks\Block\Template;

interface FrontBlocksInterface
{
    public function getCreator():CreatorInterface;

    public function getRenderer(): RendererInterface;

    public function getTwig(): TwigInterface;

    public function getLoader(): LoaderInterface;

    public function getResourceCreator(): ResourceCreator;

    public function getTemplate(): Template;
}
