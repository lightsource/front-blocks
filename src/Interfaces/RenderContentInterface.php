<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

interface RenderContentInterface
{
    public function getRenderContent(BlockInterface $block, string $content): string;
}
