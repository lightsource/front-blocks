<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

interface SettingsInterface
{
    public function addBlocksFolder(string $namespace, string $folder): void;

    public function setTwigArgs(array $twigArgs): void;

    public function setTwigExtension(string $twigExtension): void;

    public function getBlockFoldersInfo(): array;

    public function getFolderByBlockClass(string $blockClass, string &$namespace): ?string;

    public function getTwigArgs(): array;

    public function getTwigExtension(): string;
}
