<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Interfaces;

interface LoaderInterface
{
    public function setBlocksSetuper(BlocksSetuperInterface $blocksSetuper): void;

    public function getLoadedBlockClasses(): array;

    public function loadAllBlocks(string $phpFilePreg = '/.php$/'): void;
}
