<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use LightSource\FrontBlocks\Interfaces\{
    BlockInterface,
    BlocksSetuperInterface,
    LoaderInterface,
    SettingsInterface
};
use DI\FactoryInterface;
use Psr\Container\ContainerInterface;

class Loader implements LoaderInterface
{
    private array $loadedBlockClasses;
    private ?ContainerInterface $container;
    private SettingsInterface $settings;
    private ?BlocksSetuperInterface $blocksSetuper;
    private float $loadTimeSec;

    public function __construct(
        SettingsInterface      $settings,
        ?ContainerInterface    $container = null,
        BlocksSetuperInterface $blockSetup = null
    )
    {
        $this->container = $container;
        $this->loadedBlockClasses = [];
        $this->settings = $settings;
        $this->blocksSetuper = $blockSetup;
        $this->loadTimeSec = 0;
    }

    protected function tryToLoadBlock(string $phpClass): bool
    {
        if (!class_exists($phpClass) ||
            !in_array(BlockInterface::class, class_implements($phpClass), true)) {
            // without any error, because php files can contain other things
            return false;
        }

        call_user_func([$phpClass, 'setup'], $this->container);

        if ($this->blocksSetuper) {
            $this->blocksSetuper->setupBlock($phpClass, $this->container);
        }

        return true;
    }

    protected function loadBlocks(string $namespace, array $phpFileNames): void
    {
        foreach ($phpFileNames as $phpFileName) {
            $phpClass = implode('\\', [$namespace, str_replace('.php', '', $phpFileName),]);

            if (!$this->tryToLoadBlock($phpClass)) {
                continue;
            }

            $this->loadedBlockClasses[] = $phpClass;
        }
    }

    protected function loadDirectory(string $directory, string $namespace, string $phpFilePreg = '/.php$/'): void
    {
        // exclude ., ..
        $fs = array_diff(scandir($directory), ['.', '..']);

        $phpFileNames = array_filter(
            $fs,
            function ($f) use ($phpFilePreg) {
                return (1 === preg_match($phpFilePreg, $f));
            },
        );
        $phpFileNames = array_values($phpFileNames);

        $subDirectoryNames = array_filter(
            $fs,
            function ($f) {
                return false === strpos($f, '.');
            },
        );
        $subDirectoryNames = array_values($subDirectoryNames);

        foreach ($subDirectoryNames as $subDirectoryName) {
            $subDirectory = implode(DIRECTORY_SEPARATOR, [$directory, $subDirectoryName]);
            $subNamespace = implode('\\', [$namespace, $subDirectoryName]);

            $this->loadDirectory($subDirectory, $subNamespace, $phpFilePreg);
        }

        $this->loadBlocks($namespace, $phpFileNames);
    }

    public function getLoadTimeSec(): float
    {
        return $this->loadTimeSec;
    }

    public function setBlocksSetuper(BlocksSetuperInterface $blocksSetuper): void
    {
        $this->blocksSetuper = $blocksSetuper;
    }

    public function getLoadedBlockClasses(): array
    {
        return $this->loadedBlockClasses;
    }

    public function loadAllBlocks(string $phpFilePreg = '/.php$/'): void
    {
        $functionStartTime = microtime(true);

        $blockFoldersInfo = $this->settings->getBlockFoldersInfo();

        foreach ($blockFoldersInfo as $namespace => $folder) {
            $this->loadDirectory($folder, $namespace, $phpFilePreg);
        }
        $functionEndTime = microtime(true);
        $this->loadTimeSec = $functionEndTime - $functionStartTime;
    }
}
