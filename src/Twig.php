<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Exception;
use LightSource\FrontBlocks\Block\Template;
use LightSource\FrontBlocks\Interfaces\SettingsInterface;
use LightSource\FrontBlocks\Interfaces\TwigInterface;
use Psr\Log\LoggerInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\Loader\LoaderInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

class Twig implements TwigInterface
{
    protected ?LoaderInterface $twigLoader;
    protected ?Environment $twigEnvironment;
    protected SettingsInterface $settings;
    protected ?LoggerInterface $logger;

    public function __construct(
        SettingsInterface $settings,
        ?LoggerInterface $logger = null,
        ?LoaderInterface $twigLoader = null
    ) {
        $this->logger          = $logger;
        $this->twigEnvironment = null;
        $this->settings        = $settings;
        $this->twigLoader      = $twigLoader;
    }

    protected function extendTwig(): void
    {
        $this->twigEnvironment->addFilter(
            new TwigFilter(
                '_merge',
                function ($source, $additional) {
                    return $this->mergeArgsRecursively($source, $additional);
                }
            )
        );
        $this->twigEnvironment->addFunction(
            new TwigFunction(
                '_include',
                function ($block, $args = []) {
                    $block = $this->mergeArgsRecursively($block, $args);

                    return $block[Template::KEY_IS_LOADED] ?
                        $this->render($block[Template::KEY_TEMPLATE], $block) :
                        '';
                }
            )
        );
    }

    public function mergeArgsRecursively(array $args1, array $args2): array
    {
        foreach ($args2 as $key => $value) {
            if (intval($key) === $key) {
                $args1[] = $value;

                continue;
            }

            // recursive sub-merge for internal arrays
            if (
                is_array($value) &&
                key_exists($key, $args1) &&
                is_array($args1[$key])
            ) {
                $value = $this->mergeArgsRecursively($args1[$key], $value);
            }

            $args1[$key] = $value;
        }

        return $args1;
    }

    public function clearNamespace(string $namespace): string
    {
        // twig uses a slash as a separator
        return str_replace('\\', '_', $namespace);
    }

    // e.g for extend a twig with adding a new filter
    public function getEnvironment(): ?Environment
    {
        return $this->twigEnvironment;
    }

    /**
     * @throws Exception
     */
    public function initialize(): void
    {
        $blockFoldersInfo = $this->settings->getBlockFoldersInfo();

        try {
            // can be already init (in tests)
            if (! $this->twigLoader) {
                $this->twigLoader = new FilesystemLoader();
                foreach ($blockFoldersInfo as $namespace => $folder) {
                    $this->twigLoader->addPath($folder, $this->clearNamespace($namespace));
                }
            }

            $this->twigEnvironment = new Environment($this->twigLoader, $this->settings->getTwigArgs());
        } catch (Exception $ex) {
            $this->twigEnvironment = null;

            throw new Exception('Fail to create twig instance, message:' . $ex->getMessage());
        }

        $this->extendTwig();
    }

    public function render(string $templateWithNamespace, array $args = [], bool $isPrint = false): string
    {
        $html = '';

        // twig isn't loaded
        if (is_null($this->twigEnvironment)) {
            return $html;
        }

        try {
            // will generate ean exception if a template doesn't exist OR broken
            // also if a var doesn't exist (if using a 'strict_variables' flag, see Twig_Environment->__construct)
            $html .= $this->twigEnvironment->render($templateWithNamespace, $args);
        } catch (Exception $ex) {
            $html = '';

            if ($this->logger) {
                $this->logger->error('Block\'s twig contains errors', [
                    'message'               => $ex->getMessage(),
                    'file'                  => $ex->getFile(),
                    'line'                  => $ex->getLine(),
                    'templateWithNamespace' => $templateWithNamespace,
                    'args'                  => $args,
                    'trace'                 => $ex->getTraceAsString(),
                ]);
            }
        }

        if ($isPrint) {
            echo $html;
        }

        return $html;
    }
}
