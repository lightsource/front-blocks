<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use Exception;
use LightSource\FrontBlocks\Block\Template;
use LightSource\FrontBlocks\Interfaces\{
    BlockInterface,
    LoaderInterface,
    RenderContentInterface,
    RendererInterface,
    ResourceCreatorInterface,
    TemplateInterface,
    TwigInterface
};

class Renderer implements RendererInterface
{
    protected TwigInterface $twig;
    protected LoaderInterface $loader;
    protected TemplateInterface $template;
    protected ResourceCreatorInterface $resourceCreator;
    protected ?RenderContentInterface $renderContent;
    protected array $usedBlockClasses;
    protected float $renderTimeSec;
    protected float $resourceCombineTimeSec;
    protected int $countOfRenders;
    protected int $countOfCombines;
    protected array $dependencyChain;

    public function __construct(
        TwigInterface $twig,
        TemplateInterface $template,
        ResourceCreatorInterface $resourceCreator,
        ?RenderContentInterface $renderContent = null
    ) {
        $this->twig                   = $twig;
        $this->template               = $template;
        $this->resourceCreator        = $resourceCreator;
        $this->renderContent          = $renderContent;
        $this->usedBlockClasses       = [];
        $this->renderTimeSec          = 0;
        $this->resourceCombineTimeSec = 0;
        $this->countOfRenders         = 0;
        $this->countOfCombines        = 0;
        $this->dependencyChain        = [];
    }

    /**
     * @throws Exception
     */
    protected function getDependencyClassesRecursively(BlockInterface $block): array
    {
        $dependencyClasses = [];
        $dependencies      = $block->getDependencies();

        $this->dependencyChain[] = get_class($block);

        foreach ($dependencies as $dependency) {
            // for IDE
            if (! $dependency instanceof BlockInterface) {
                continue;
            }
            $dependencyClass = get_class($dependency);

            if (in_array($dependencyClass, $this->dependencyChain, true)) {
                throw new Exception(
                    'Block dependency error.' .
                    'The next getDependencies() call (' . $dependencyClass . ') will run a recursion, current classes chain is :'
                    . print_r($this->dependencyChain, true)
                );
            }

            $dependencyClasses = array_merge($dependencyClasses, $this->getDependencyClassesRecursively($dependency));
            // at the end to keep the right order
            $dependencyClasses[] = $dependencyClass;
            $dependencyClasses   = array_unique($dependencyClasses);
            $dependencyClasses   = array_values($dependencyClasses);
        }

        array_splice($this->dependencyChain, count($this->dependencyChain) - 1, 1);

        return $dependencyClasses;
    }

    // for tests
    public function getDependencyChain(): array
    {
        return $this->dependencyChain;
    }

    public function getUsedBlockClasses(): array
    {
        return $this->usedBlockClasses;
    }

    public function getRenderTimeSec(): float
    {
        return $this->renderTimeSec;
    }

    public function getResourceCombineTimeSec(): float
    {
        return $this->resourceCombineTimeSec;
    }

    public function getCountOfRenders(): int
    {
        return $this->countOfRenders;
    }

    public function getCountOfCombines(): int
    {
        return $this->countOfCombines;
    }

    public function setRenderContent(RenderContentInterface $renderContent): void
    {
        $this->renderContent = $renderContent;
    }

    /**
     * @throws Exception
     */
    public function getUsedResources(
        string $extension,
        bool $isIncludeSource = false,
        array $excludeBlocks = []
    ): string {
        $functionStartTime = microtime(true);
        $resourcesContent  = '';

        $usedBlocks = array_diff($this->usedBlockClasses, $excludeBlocks);

        foreach ($usedBlocks as $usedBlockClass) {
            $resource = $this->resourceCreator->createResource($usedBlockClass);

            $pathToResourceFile = $resource->getFolder() . DIRECTORY_SEPARATOR .
                                  $resource->getRelativePath() . $extension;

            if (! is_file($pathToResourceFile)) {
                continue;
            }

            $resourceContent = file_get_contents($pathToResourceFile);
            $resourceContent = trim($resourceContent);

            // ignore empty files
            if (! $resourceContent) {
                continue;
            }

            $resourcesContent .= $isIncludeSource ?
                "\n/* " . $resource->getName() . " */\n" :
                '';

            $resourcesContent .= $resourceContent;
        }

        $functionEndTime              = microtime(true);
        $this->resourceCombineTimeSec += $functionEndTime - $functionStartTime;
        $this->countOfCombines++;

        return $resourcesContent;
    }

    /**
     * @throws Exception
     */
    public function render(BlockInterface $block, array $args = [], bool $isPrint = false): string
    {
        $functionStartTime = microtime(true);
        $dependencies      = $this->getDependencyClassesRecursively($block);
        // at the end to keep the right order
        $dependencies[]         = get_class($block);
        $newDependencies        = array_diff($dependencies, $this->usedBlockClasses);
        $this->usedBlockClasses = array_merge($this->usedBlockClasses, $newDependencies);

        $templateArgs = $this->template->getArgs($block);

        $templateArgs           = $this->twig->mergeArgsRecursively($templateArgs, $args);

        // log already exists
        if (! $templateArgs[Template::KEY_TEMPLATE]) {
            return '';
        }

        $content = $this->twig->render($templateArgs[Template::KEY_TEMPLATE], $templateArgs);
        $content = $this->renderContent ?
            $this->renderContent->getRenderContent($block, $content) :
            $content;

        if ($isPrint) {
            echo $content;
        }

        $functionEndTime     = microtime(true);
        $this->renderTimeSec += $functionEndTime - $functionStartTime;
        $this->countOfRenders++;

        return $content;
    }
}
