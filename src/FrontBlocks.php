<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use LightSource\FrontBlocks\{
    Block\Creator,
    Block\ResourceCreator,
    Block\Template,
    Interfaces\CreatorInterface,
    Interfaces\FrontBlocksInterface,
    Interfaces\LoaderInterface,
    Interfaces\RendererInterface,
    Interfaces\SettingsInterface,
    Interfaces\TwigInterface
};
use DI\FactoryInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Exception;

class FrontBlocks implements FrontBlocksInterface
{
    protected RendererInterface $renderer;
    protected TwigInterface $twig;
    protected LoaderInterface $loader;
    protected ResourceCreator $resourceCreator;
    protected Template $template;
    protected CreatorInterface $creator;

    /**
     * @throws Exception
     */
    public function __construct(
        SettingsInterface $settings,
        ?ContainerInterface $container = null,
        LoggerInterface $logger = null
    ) {
        $this->resourceCreator = new ResourceCreator($settings);

        $this->twig = new Twig($settings, $logger);
        $this->twig->initialize();
        $this->creator   = new Creator($container);
        $this->template  = new Template($settings, $this->twig, $this->resourceCreator);
        $this->renderer  = new Renderer($this->twig, $this->template, $this->resourceCreator);
        $this->loader    = new Loader($settings, $container);
    }

    public function getCreator(): CreatorInterface
    {
        return $this->creator;
    }

    public function getRenderer(): RendererInterface
    {
        return $this->renderer;
    }

    public function getTwig(): TwigInterface
    {
        return $this->twig;
    }

    public function getLoader(): LoaderInterface
    {
        return $this->loader;
    }

    public function getResourceCreator(): ResourceCreator
    {
        return $this->resourceCreator;
    }

    public function getTemplate(): Template
    {
        return $this->template;
    }
}
