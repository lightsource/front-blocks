<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use LightSource\FrontBlocks\Interfaces\SettingsInterface;

class Settings implements SettingsInterface
{
    protected array $blockFoldersInfo;
    protected array $twigArgs;
    protected string $twigExtension;

    public function __construct()
    {
        $this->blockFoldersInfo = [];
        $this->twigArgs         = [
            // will generate exception if a var doesn't exist instead of replace to NULL
            'strict_variables' => true,
            // disable autoescape to prevent break data
            'autoescape'       => false,
        ];
        $this->twigExtension    = '.twig';
    }

    public function addBlocksFolder(string $namespace, string $folder): void
    {
        $this->blockFoldersInfo[$namespace] = $folder;
    }

    public function setTwigArgs(array $twigArgs): void
    {
        $this->twigArgs = array_merge($this->twigArgs, $twigArgs);
    }

    public function setTwigExtension(string $twigExtension): void
    {
        $this->twigExtension = $twigExtension;
    }

    public function getBlockFoldersInfo(): array
    {
        return $this->blockFoldersInfo;
    }

    public function getFolderByBlockClass(string $blockClass, string &$namespace): ?string
    {
        $folder = null;

        foreach ($this->blockFoldersInfo as $blockNamespace => $blockFolder) {
            if (0 !== strpos($blockClass, $blockNamespace)) {
                continue;
            }

            $folder    = $blockFolder;
            $namespace = $blockNamespace;
            break;
        }

        return $folder;
    }

    public function getTwigArgs(): array
    {
        return $this->twigArgs;
    }

    public function getTwigExtension(): string
    {
        return $this->twigExtension;
    }
}
