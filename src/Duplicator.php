<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks;

use LightSource\FrontBlocks\Block\ResourceCreator;
use LightSource\FrontBlocks\Interfaces\ResourceInterface;

class Duplicator
{
    protected string $blocksFolder;
    protected string $sourceBlockFile;
    protected string $targetBlockFile;
    protected array $files;
    protected ResourceCreator $resourceCreator;

    public function __construct(
        string $blocksFolder,
        string $sourceBlockFile,
        string $targetBlockFile,
        ResourceCreator $resourceCreator
    ) {
        $this->blocksFolder    = $blocksFolder;
        $this->sourceBlockFile = $sourceBlockFile;
        $this->targetBlockFile = $targetBlockFile;
        $this->files           = [];
        $this->resourceCreator = $resourceCreator;
    }

    protected function getBlocksPath(string $target): string
    {
        return implode(DIRECTORY_SEPARATOR, [$this->blocksFolder, $target,]);
    }

    protected function copyFolderFiles(string $source, string $target): bool
    {
        $isCopied = false;

        if (! is_dir($source) ||
            is_dir($target)) {
            return $isCopied;
        }

        mkdir($target, 0777, true);

        $sourceHandle = opendir($source);

        while (($fileName = readdir($sourceHandle))) {
            if (in_array($fileName, ['.', '..',], true)) {
                continue;
            }

            $sourceFile = $source . DIRECTORY_SEPARATOR . $fileName;
            $targetFile = $target . DIRECTORY_SEPARATOR . $fileName;

            if (is_dir($sourceFile)) {
                continue;
            }

            copy($sourceFile, $targetFile);

            $this->files[] = $targetFile;
        }

        closedir($sourceHandle);

        return true;
    }

    protected function getBlockClass(string $blockFile): string
    {
        $controllerClass = str_replace('/', '\\', $blockFile);

        return str_replace('.php', '', $controllerClass);
    }

    protected function getBlockResourceInfo(string $blockClass): ResourceInterface
    {
        return $this->resourceCreator->getResourceForDuplicator($blockClass);
    }

    protected function getBlockNamespace(string $blockClass): string
    {
        $namespace = explode('\\', $blockClass);
        $namespace = array_slice($namespace, 0, count($namespace) - 1);

        return implode('\\', $namespace);
    }

    protected function replaceNameInFiles(string $originName, string $targetName)
    {
        for ($i = 0; $i < count($this->files); $i++) {
            $fileName    = basename($this->files[$i]);
            $parentPath  = dirname($this->files[$i]);
            $newFileName = preg_replace("/{$originName}/", $targetName, $fileName);

            if ($newFileName === $fileName) {
                continue;
            }

            $pathToNewFile = $parentPath . DIRECTORY_SEPARATOR . $newFileName;

            rename($parentPath . DIRECTORY_SEPARATOR . $fileName, $pathToNewFile);

            $this->files[$i] = $pathToNewFile;
        }
    }

    protected function replaceContentInFiles(string $originContent, string $targetContent): void
    {
        foreach ($this->files as $pathToFile) {
            $fileContent = file_get_contents($pathToFile);

            $newFileContent = preg_replace("/{$originContent}/", $targetContent, $fileContent);

            if ($newFileContent === $fileContent) {
                continue;
            }

            file_put_contents($pathToFile, $newFileContent);
        }
    }

    protected function replace(string $sourceBlockClass, string $targetBlockClass): void
    {
        $sourceResourceName = $this->getBlockResourceInfo($sourceBlockClass)->getName();
        $targetResourceName = $this->getBlockResourceInfo($targetBlockClass)->getName();

        $sourceBlockNamespace = addslashes($this->getBlockNamespace($sourceBlockClass));
        $targetBlockNamespace = addslashes($this->getBlockNamespace($targetBlockClass));

        $this->replaceNameInFiles($sourceResourceName, $targetResourceName);
        // blockName
        $this->replaceContentInFiles($sourceResourceName, $targetResourceName);
        // blockName in lower case, e.g. a css class name
        $this->replaceContentInFiles(strtolower($sourceResourceName), strtolower($targetResourceName));
        // blockNamespace
        $this->replaceContentInFiles($sourceBlockNamespace, $targetBlockNamespace);
    }

    public function copy(bool $isWithReplace): bool
    {
        $sourceFolder = $this->getBlocksPath(dirname($this->sourceBlockFile));
        $targetFolder = $this->getBlocksPath(dirname($this->targetBlockFile));

        if (! $this->copyFolderFiles($sourceFolder, $targetFolder)) {
            return false;
        }

        if (! $isWithReplace) {
            return true;
        }

        $sourceBlockClass = $this->getBlockClass($this->sourceBlockFile);
        $targetBlockClass = $this->getBlockClass($this->targetBlockFile);

        $this->replace($sourceBlockClass, $targetBlockClass);

        return true;
    }
}
