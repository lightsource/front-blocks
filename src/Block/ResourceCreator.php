<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Block;

use Exception;
use LightSource\FrontBlocks\Interfaces\{
    BlockInterface,
    ResourceCreatorInterface,
    ResourceInterface,
    SettingsInterface
};

class ResourceCreator implements ResourceCreatorInterface
{
    protected ?SettingsInterface $settings;

    // can be null only for the Duplicator class
    public function __construct(?SettingsInterface $settings)
    {
        $this->settings = $settings;
    }

    protected function createEmptyResource(): ResourceInterface
    {
        return new Resource();
    }

    protected function isBlock(string $blockClass): bool
    {
        return class_exists($blockClass) &&
               in_array(BlockInterface::class, class_implements($blockClass), true);
    }

    protected function fillResource(string $blockClass, ResourceInterface $blockResource): ResourceInterface
    {
        //  e.g. Example/Theme/Main/ExampleThemeMain
        $relativeBlockNamespace = str_replace(
            $blockResource->getNamespace() . '\\',
            '',
            $blockClass
        );

        // e.g. ExampleThemeMain
        $blockName = explode('\\', $relativeBlockNamespace);
        $blockName = $blockName[count($blockName) - 1];

        // e.g. Example/Theme/Main
        $relativePath = explode('\\', $relativeBlockNamespace);
        $relativePath = array_slice($relativePath, 0, count($relativePath) - 1);
        $relativePath = implode(DIRECTORY_SEPARATOR, $relativePath);

        $blockResource->setRelativePath($relativePath . DIRECTORY_SEPARATOR . $blockName);
        $blockResource->setRelativeFolderPath($relativePath);
        $blockResource->setName($blockName);

        return $blockResource;
    }

    // inner, only for the Duplicator class
    public function getResourceForDuplicator(string $blockClass): ResourceInterface
    {
        $resource = $this->createEmptyResource();
        $resource->setNamespace('namespace');

        return $this->fillResource('namespace\\' . $blockClass, $resource);
    }

    /**
     * @throws Exception
     */
    public function createResource(string $blockClass): ResourceInterface
    {
        $blockResource = $this->createEmptyResource();

        if (! $this->isBlock($blockClass)) {
            throw new Exception('Block class is not a block, class: ' . $blockClass);
        }

        $blockNamespace = '';
        $blockFolder    = $this->settings->getFolderByBlockClass($blockClass, $blockNamespace);

        if (! $blockFolder) {
            throw new Exception('Block has the non registered namespace, class: ' . $blockClass);
        }

        $blockResource->setNamespace($blockNamespace);
        $blockResource->setFolder($blockFolder);

        return $this->fillResource($blockClass, $blockResource);
    }
}
