<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Block;

use Exception;
use LightSource\FrontBlocks\Interfaces\BlockInterface;
use LightSource\FrontBlocks\Interfaces\CreatorInterface;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionProperty;

class Creator implements CreatorInterface
{
    protected ?ContainerInterface $container;
    protected array $creationChain;

    public function __construct(?ContainerInterface $container)
    {
        $this->container = $container;
        $this->creationChain = [];
    }

    /**
     * @throws Exception
     */
    protected function initializeDependencies(BlockInterface $block): void
    {
        $properties = $block->getFieldsInfo();

        foreach ($properties as $propertyName => $propertyType) {
            if (!class_exists($propertyType) ||
                !in_array(BlockInterface::class, class_implements($propertyType), true)) {
                continue;
            }

            try {
                $property = new ReflectionProperty(get_class($block), $propertyName);
            } catch (Exception $exception) {
                throw new Exception(
                    'Auto initialization of the block property is failed, class: '
                    . get_class($block)
                    . ', property: ' . $propertyName
                );
            }

            // allows getValue/setValue on protected
            $property->setAccessible(true);

            // supporting of polymorphism
            if ($property->isInitialized($block) &&
                !is_null($property->getValue($block))) {
                $propertyType = get_class($property->getValue($block));
            }

            $value = $this->create($propertyType);

            $property->setValue($block, $value);
        }
    }

    protected function getConstructorArguments(ReflectionClass $classInfo): array
    {
        $constructorArguments = [];

        $constructorInfo = $classInfo->getConstructor();
        $constructorParametersInfo = $constructorInfo ?
            $constructorInfo->getParameters() :
            [];
        foreach ($constructorParametersInfo as $constructorParameterInfo) {
            $parameterClass = $constructorParameterInfo->getType();
            $parameterClass = $parameterClass ?
                $parameterClass->getName() :
                '';

            $parameterClass = (class_exists($parameterClass) ||
                interface_exists($parameterClass)) ?
                $parameterClass :
                '';

            $constructorArguments[] = $parameterClass;
        }

        return $constructorArguments;
    }

    // for tests
    public function getCreationChain(): array
    {
        return $this->creationChain;
    }

    /**
     * @template T
     *
     * @param class-string<T> $blockClass
     *
     * @return T
     * @throws Exception
     */
    public function create(string $blockClass): BlockInterface
    {
        if (!class_exists($blockClass) ||
            !in_array(BlockInterface::class, class_implements($blockClass), true)) {
            throw new Exception('Block class is not a block, class:' . $blockClass);
        }

        if (in_array($blockClass, $this->creationChain)) {
            throw new Exception(
                'Fail to create a block instance.' .
                'The next block constructor (' . $blockClass . ') will run a recursion, current classes chain is :'
                . print_r($this->creationChain, true)
            );
        }

        $this->creationChain[] = $blockClass;

        try {
            $classInfo = new ReflectionClass($blockClass);
        } catch (Exception $exception) {
            throw new Exception('Auto creation of the block is failed, class: ' . $blockClass);
        }

        $constructorArguments = $this->getConstructorArguments($classInfo);
        $constructorData = [];

        if ($constructorArguments &&
            $this->container) {
            foreach ($constructorArguments as $constructorArgument) {
                // will throw Exception if it's missing
                $constructorData[] = $this->container->get($constructorArgument);
            }
        }

        try {
            /** @var Block $block */
            $block = $constructorData ?
                $classInfo->newInstanceArgs($constructorData) :
                $classInfo->newInstance();
        } catch (Exception $exception) {
            throw new Exception('Auto creation of the block is failed, class: ' . $blockClass);
        }

        $this->initializeDependencies($block);

        array_splice($this->creationChain, count($this->creationChain) - 1, 1);

        return $block;
    }
}
