<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Block;

use LightSource\FrontBlocks\Interfaces\ResourceInterface;

class Resource implements ResourceInterface
{
    protected string $namespace;
    protected string $folder;
    protected string $relativeFolderPath;
    protected string $relativePath;
    protected string $name;

    public function __construct()
    {
        $this->namespace          = '';
        $this->folder             = '';
        $this->relativeFolderPath = '';
        $this->relativePath       = '';
        $this->name               = '';
    }

    public function setNamespace(string $namespace): void
    {
        $this->namespace = $namespace;
    }

    public function setFolder(string $folder): void
    {
        $this->folder = $folder;
    }

    public function setRelativeFolderPath(string $relativeFolderPath): void
    {
        $this->relativeFolderPath = $relativeFolderPath;
    }

    public function setRelativePath(string $relativePath): void
    {
        $this->relativePath = $relativePath;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getNamespace(): string
    {
        return $this->namespace;
    }

    public function getFolder(): string
    {
        return $this->folder;
    }

    public function getRelativeFolderPath(): string
    {
        return $this->relativeFolderPath;
    }

    public function getRelativePath(): string
    {
        return $this->relativePath;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
