<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Block;

use Exception;
use LightSource\FrontBlocks\Interfaces\{
    BlockInterface,
    ResourceCreatorInterface,
    ResourceInterface,
    SettingsInterface,
    TemplateInterface,
    TwigInterface
};

class Template implements TemplateInterface
{
    public const KEY_TEMPLATE = '_template';
    public const KEY_IS_LOADED = '_isLoaded';
    public const KEY_PARENT = '_parentTemplate';

    protected SettingsInterface $settings;
    protected ResourceCreatorInterface $resourceCreator;
    protected TwigInterface $twig;
    protected array $argsChain;

    public function __construct(
        SettingsInterface $settings,
        TwigInterface $twig,
        ResourceCreatorInterface $resourceCreator
    ) {
        $this->settings        = $settings;
        $this->resourceCreator = $resourceCreator;
        $this->twig            = $twig;
        $this->argsChain       = [];
    }

    protected function isFileExist(string $file): bool
    {
        return is_file($file);
    }

    protected function getParentClass(string $blockClass): ?string
    {
        $parentClass = get_parent_class($blockClass);

        if (! $parentClass ||
            ! in_array(BlockInterface::class, class_implements($parentClass), true) ||
            $parentClass === Block::class) {
            return null;
        }

        return $parentClass;
    }

    /**
     * @throws Exception
     */
    protected function getResourceRecursivelyWhereTwigExist(string $blockClass): ?ResourceInterface
    {
        $blockResource = $this->resourceCreator->createResource($blockClass);

        $absTwigPath = implode(
            '',
            [
                $blockResource->getFolder(),
                DIRECTORY_SEPARATOR,
                $blockResource->getRelativePath(),
                $this->settings->getTwigExtension(),
            ]
        );

        if ($this->isFileExist($absTwigPath)) {
            return $blockResource;
        }

        $parentClass = $this->getParentClass($blockClass);

        return $parentClass ?
            $this->getResourceRecursivelyWhereTwigExist($parentClass) :
            null;
    }

    protected function getTwigRelativeTemplatePath(ResourceInterface $resource): string
    {
        return $resource->getRelativePath() . $this->settings->getTwigExtension();
    }

    /**
     * @throws Exception
     */
    protected function getParentTwigTemplatePathWithNamespace(BlockInterface $block): string
    {
        $parentClass          = $this->getParentClass(get_class($block));
        $parentClassNamespace = '';

        // necessary checking that the parent class has the registered namespace, it's okay when it isn't true
        // (e.g. parent is the abstract Block class or any else)
        if (! $parentClass ||
            ! $this->settings->getFolderByBlockClass($parentClass, $parentClassNamespace)) {
            return '';
        }

        $parentBlockResource = $this->getResourceRecursivelyWhereTwigExist($parentClass);

        $parentBlockTemplate = $parentBlockResource ?
            $this->getTwigRelativeTemplatePath($parentBlockResource) :
            '';

        return $parentBlockTemplate ?
            '@' . $this->twig->clearNamespace($parentBlockResource->getNamespace()) . '/' . $parentBlockTemplate :
            '';
    }

    // for tests
    public function getArgsChain(): array
    {
        return $this->argsChain;
    }

    /**
     * @throws Exception
     */
    public function getArgs(BlockInterface $block): array
    {
        $blockResource = $this->getResourceRecursivelyWhereTwigExist(get_class($block));

        if (! $blockResource) {
            return [];
        }

        $this->argsChain[] = get_class($block);

        $templateArgs = [
            self::KEY_TEMPLATE  => '@' . $this->twig->clearNamespace($blockResource->getNamespace()) .
                                   '/' . $this->getTwigRelativeTemplatePath($blockResource),
            self::KEY_IS_LOADED => $block->isLoaded(),
            self::KEY_PARENT    => $this->getParentTwigTemplatePathWithNamespace($block),
        ];

        $templateArgs = array_merge($templateArgs, $block->getTemplateArgs());

        foreach ($templateArgs as $fieldName => $value) {
            if (is_object($value) &&
                in_array(BlockInterface::class, class_implements($value), true)) {
                if (in_array(get_class($value), $this->argsChain, true)) {
                    throw new Exception(
                        'Error during getting block\'s args.' .
                        'The next getArgs call (' . get_class(
                            $value
                        ) . ') will run a recursion, current classes chain is :'
                        . print_r($this->argsChain, true)
                    );
                }

                $value = $this->getArgs($value);
            }

            if (is_array($value) &&
                isset($value[0]) &&
                is_object($value[0]) &&
                in_array(BlockInterface::class, class_implements($value[0]), true)) {
                foreach ($value as &$blockItem) {
                    if (! is_object($blockItem) ||
                        ! in_array(BlockInterface::class, class_implements($blockItem), true)) {
                        continue;
                    }

                    if (in_array(get_class($blockItem), $this->argsChain, true)) {
                        throw new Exception(
                            'Error during getting block\'s args.' .
                            'The next getArgs call (' . get_class(
                                $blockItem
                            ) . ') will run a recursion, current classes chain is :'
                            . print_r($this->argsChain, true)
                        );
                    }

                    $blockItem = $this->getArgs($blockItem);
                }
            }

            $templateArgs[$fieldName] = $value;
        }

        array_splice($this->argsChain, count($this->argsChain) - 1, 1);

        return $templateArgs;
    }
}
