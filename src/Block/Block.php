<?php

declare(strict_types=1);

namespace LightSource\FrontBlocks\Block;

use Exception;
use LightSource\FrontBlocks\Interfaces\BlockInterface;
use LightSource\FrontBlocks\Interfaces\ExternalDependenciesInterface;
use Psr\Container\ContainerInterface;
use ReflectionProperty;

abstract class Block implements BlockInterface
{
    private array $fieldsInfo;
    private bool $isLoaded;

    public function __construct()
    {
        $this->fieldsInfo = [];
        $this->isLoaded   = false;

        $this->readFieldsInfo();
        $this->autoInitFields();
    }

    protected function getFieldType(string $fieldName): ?string
    {
        $fieldType = null;

        try {
            // used static for child support
            $property = new ReflectionProperty(static::class, $fieldName);
        } catch (Exception $ex) {
            return $fieldType;
        }

        if (! $property->isProtected()) {
            return $fieldType;
        }

        return $property->getType() ?
            $property->getType()->getName() :
            '';
    }

    protected function readFieldsInfo(): void
    {
        $fieldNames = array_keys(get_class_vars(static::class));

        foreach ($fieldNames as $fieldName) {
            $fieldType = $this->getFieldType($fieldName);

            // only protected fields
            if (is_null($fieldType)) {
                continue;
            }

            $this->fieldsInfo[$fieldName] = $fieldType;
        }
    }

    protected function autoInitFields(): void
    {
        $fieldInfo = $this->getFieldsInfo();
        foreach ($fieldInfo as $fieldName => $fieldType) {
            // ignore fields without a type
            if (! $fieldType) {
                continue;
            }

            $defaultValue = null;

            switch ($fieldType) {
                case 'int':
                case 'float':
                    $defaultValue = 0;
                    break;
                case 'bool':
                    $defaultValue = false;
                    break;
                case 'string':
                    $defaultValue = '';
                    break;
                case 'array':
                    $defaultValue = [];
                    break;
            }

            if (is_null($defaultValue)) {
                continue;
            }

            $this->{$fieldName} = $defaultValue;
        }
    }

    protected function load(): void
    {
        $this->isLoaded = true;
    }

    protected function getBlockField(string $fieldName): ?BlockInterface
    {
        if (! key_exists($fieldName, $this->getFieldsInfo())) {
            return null;
        }

        $block = $this->{$fieldName};

        if (! is_object($block) ||
            ! in_array(BlockInterface::class, class_implements($block), true)) {
            return null;
        }

        return $block;
    }

    public static function setup(?ContainerInterface $container): void
    {
    }

    public function getFieldsInfo(): array
    {
        return $this->fieldsInfo;
    }

    public function isLoaded(): bool
    {
        return $this->isLoaded;
    }

    public function getDependencies(): array
    {
        $dependencies = [];
        $fieldsInfo   = $this->getFieldsInfo();

        foreach ($fieldsInfo as $fieldName => $fieldType) {
            $dependencyBlock = $this->getBlockField($fieldName);

            if (! $dependencyBlock) {
                continue;
            }

            $dependencies[] = $dependencyBlock;
        }

        return $dependencies;
    }

    public function getTemplateArgs(): array
    {
        $templateArgs = [];

        $fieldsInfo = $this->getFieldsInfo();

        foreach ($fieldsInfo as $fieldName => $fieldType) {
            $templateArgs[$fieldName] = $this->{$fieldName};
        }

        return $templateArgs;
    }

    /**
     * get deeps clone unlike the std 'clone'
     * @return static
     */
    public function getDeepClone(): BlockInterface
    {
        $clone = clone $this;

        $fieldsInfo = $clone->getFieldsInfo();
        foreach ($fieldsInfo as $fieldName => $fieldType) {
            if (! ($clone->{$fieldName} instanceof BlockInterface)) {
                continue;
            }

            $clone->{$fieldName} = $clone->{$fieldName}->getDeepClone();
        }

        return $clone;
    }
}
